<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ParkingResource;
use App\Models\Parking;
use App\Services\ParkingPriceService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
* @group Parking
*/
class ParkingController extends Controller
{
    public function index()
    {
        $parkings = Parking::active()->latest('start_time')->get();
        return ParkingResource::collection($parkings);
    }

    public function start(Request $request)
    {
        $request->validate([
            'vehicle_id' => ['required', 'integer', 'exists:vehicles,id'],
            'zone_id' => ['required', 'integer', 'exists:zones,id']
        ]);

        if(Parking::active()->where('vehicle_id', $request->vehicle_id)->exists()) {
            return response()->json([
                'errors' => ['general' => ['Can\'t start parking twice using same vehicle. Please stop currently active parking']]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $parking = Parking::create([
            'user_id' => auth()->id(),
            'vehicle_id' => $request->vehicle_id,
            'zone_id' => $request->zone_id,
            'start_time' => now()
        ]);

        $parking->load('vehicle', 'zone');

        return ParkingResource::make($parking);
    }

    public function show(Parking $parking)
    {
        return ParkingResource::make($parking);
    }

    public function stop(Parking $parking)
    {
        $parking->update([
            'stop_time' => now(),
            'total_price' => ParkingPriceService::calculatePrice($parking->zone_id, $parking->start_time)
        ]);

        return ParkingResource::make($parking);
    }

    public function history_parking()
    {
        $parkings = Parking::stopped()->with(['vehicle'])->latest('stop_time')->get();
        return ParkingResource::collection($parkings);
    }
}
